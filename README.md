<a href="https://flathub.org/apps/details/com.jvieira.tpt.Metronome">
<img src="https://flathub.org/assets/badges/flathub-badge-i-en.png" width="190px" />
</a>

# Metronome

A simple Metronome application for the GNOME desktop.


## Screenshots
<div align="center">
![Screenshot](data/screenshots/metronome.png)
</div>

## Building
### Building with GNOME Builder + Flatpak
Metronome can be built and run with [GNOME Builder](https://wiki.gnome.org/Apps/Builder) >= 3.28.
Just clone the repo and hit the run button!

### Building it manually
```
git clone https://gitlab.gnome.org/jvieira.tpt/metronome.git
cd metronome
mkdir build
cd build
meson ..
ninja
sudo ninja install
```

Dependencies:
 - GTK 3.22 or later
 - GStreamer 1.0
 - libhandy 0.0

## Code Of Conduct
We follow the [GNOME Code of Conduct](/CODE_OF_CONDUCT.mdmd).
All communications in project spaces are expected to follow it.

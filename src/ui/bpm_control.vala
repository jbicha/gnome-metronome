/* bpm_control.vala
 *
 * Copyright 2020 João Vieira <jvieira.tpt@gmail.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 */


[GtkTemplate (ui="/com/jvieira/tpt/Metronome/ui/bpm_control.ui")]
public class Metronome.BPMControlBox : Gtk.Box {
    [GtkChild]
    private Gtk.Label bpm_label;

    public uint bpm { get; set; default = 100; }

    [GtkCallback]
    private void on_inc_button_clicked_cb () {
        bpm++;
    }

    [GtkCallback]
    private void on_dec_button_clicked_cb () {
        bpm--;
    }

    [GtkCallback]
    private void on_bpm_changed_cb () {
        bpm_label.label = "%g".printf (bpm);
    }
}

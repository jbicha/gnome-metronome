/* rhythm_box.vala
 *
 * Copyright 2020 João Vieira <jvieira.tpt@gmail.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 */


[GtkTemplate (ui="/com/jvieira/tpt/Metronome/ui/rhythm_box.ui")]
public class Metronome.RhythmBox : Gtk.Grid {
    public uint bpb { get; set; }
    public uint cpb { get; set; }

    [GtkChild]
    private Gtk.RadioButton two_four_button;
    [GtkChild]
    private Gtk.RadioButton three_four_button;
    [GtkChild]
    private Gtk.RadioButton four_four_button;
    [GtkChild]
    private Gtk.RadioButton six_eight_button;

    construct {
        two_four_button.clicked.connect (() => {
            bpb = 2;
            cpb = 1;
        });
        three_four_button.clicked.connect (() => {
            bpb = 3;
            cpb = 1;
        });
        four_four_button.clicked.connect (() => {
            bpb = 4;
            cpb = 1;
        });
        six_eight_button.clicked.connect (() => {
            bpb = 2;
            cpb = 3;
        });
    }
}

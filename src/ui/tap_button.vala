/* tap_button.vala
 *
 * Copyright 2020 João Vieira <jvieira.tpt@gmail.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 */


[GtkTemplate (ui="/com/jvieira/tpt/Metronome/ui/tap_button.ui")]
public class Metronome.TapButton : Gtk.Bin {
    [GtkChild]
    private Gtk.Button tap_button;

    private GLib.DateTime begin = null;
    private GLib.DateTime end = null;

    public uint bpm { get; set; }
    public string label { set { tap_button.label = value; } }

    [GtkCallback]
    private void on_tap_button_clicked_cb () {
        begin = end;
        end = new GLib.DateTime.now ();

        if (begin != null) {
            var diff = end.difference (begin);
            bpm = (uint) (60000000 / diff);
            tap_button.label = "%g".printf (bpm);
            if (bpm < 20) {
                bpm = 20;
            }
            else if (bpm > 260) {
                bpm = 260;
            }
        }
    }

    public void reset () {
        begin = null;
        end = null;
        label = "TAP";
    }
}

/* window.vala
 *
 * Copyright 2020 João Vieira <jvieira.tpt@gmail.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 */


[GtkTemplate (ui="/com/jvieira/tpt/Metronome/ui/window.ui")]
public class Metronome.Window : Gtk.ApplicationWindow {
    public uint bpm { get; set; default = 100; }
    public uint bpb { get; set; default = 4; }
    public uint cpb { get; set; default = 1; }
    [GtkChild]
    private Metronome.HeaderBar headerbar;
    [GtkChild]
    private Metronome.MediaControlBox main_box;
    private const ActionEntry[] action_entries = {
        { "shortcuts", shortcuts },
    };

    public Window (Application app) {
        Object (
            application: app
        );
    }

    construct {
        add_action_entries (action_entries, this);
    }

    [GtkCallback]
    private bool on_key_pressed (Gdk.EventKey event) {
        var default_mod_mask = Gtk.accelerator_get_default_mod_mask ();

        if ((event.keyval == Gdk.Key.plus) &&
           ((event.state & default_mod_mask) == (Gdk.ModifierType.CONTROL_MASK | Gdk.ModifierType.SHIFT_MASK))) {
            if (bpm < 260) {
                bpm++;
            }
            return true;
        }
        else if ((event.keyval == Gdk.Key.minus) &&
                ((event.state & default_mod_mask) == Gdk.ModifierType.CONTROL_MASK)) {
            if (bpm > 20) {
                bpm--;
            }
            return true;
        }
        else if ((event.keyval == Gdk.Key.p || event.keyval == Gdk.Key.P) &&
                ((event.state & default_mod_mask) == Gdk.ModifierType.CONTROL_MASK)) {
            main_box.on_play_button_clicked_cb ();
            return true;
        }
        else if (event.keyval == Gdk.Key.F10) {
            headerbar.menu_button.clicked ();
            return true;
        }
        else if ((event.keyval == Gdk.Key.question) &&
                ((event.state & default_mod_mask) == (Gdk.ModifierType.CONTROL_MASK | Gdk.ModifierType.SHIFT_MASK))) {
            shortcuts ();
            return true;
        }

        return false;
    }

    private void shortcuts () {
        Metronome.ShortcutsWindow shortcuts_window = new Metronome.ShortcutsWindow ();
        shortcuts_window.transient_for = this;

        shortcuts_window.present_with_time (Gtk.get_current_event_time ());
    }
}

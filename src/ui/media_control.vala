/* media_control.vala
 *
 * Copyright 2020 João Vieira <jvieira.tpt@gmail.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 */


[GtkTemplate (ui="/com/jvieira/tpt/Metronome/ui/media_control.ui")]
public class Metronome.MediaControlBox : Gtk.Box {
    [GtkChild]
    private Gtk.Button play_button;
    [GtkChild]
    private Gtk.VolumeButton volume_button;
    [GtkChild]
    private Metronome.TapButton tap_button;
    [GtkChild]
    private Gtk.Image play_image;
    [GtkChild]
    private Gtk.Image stop_image;

    private enum State {
        PLAYING,
        STOPPED
    }

    private double progress;
    private State state = State.STOPPED;
    private uint timeout_id;
    public uint beat { get; set; default = 0; }
    public uint bpb { get; set; default = 4; }
    public uint cpb { get; set; default = 1;}
    public uint bpm { get; set; default = 100; }
    public double volume { get; set; }

    private Metronome.Timer timer;
    private GLib.Timer progress_timer;

    public signal void start_play ();
    public signal void stop_play ();

    construct {
        timer = new Metronome.Timer ();
        progress_timer = new GLib.Timer ();

        timer.bind_property ("beat", this, "beat", BindingFlags.BIDIRECTIONAL);
        timer.bind_property ("bpm", this, "bpm", BindingFlags.BIDIRECTIONAL);
        timer.bind_property ("bpb", this, "bpb", BindingFlags.BIDIRECTIONAL);
        timer.bind_property ("cpb", this, "cpb", BindingFlags.BIDIRECTIONAL);
        timer.bind_property ("volume", this, "volume", BindingFlags.BIDIRECTIONAL);
    }

    [GtkCallback]
    public void on_play_button_clicked_cb () {
        if (state == State.STOPPED) {
            start_play ();
            play_button.image = stop_image;
            tap_button.reset ();
        }
        else if (state == State.PLAYING) {
            stop_play ();
            play_button.image = play_image;
            tap_button.reset ();
        }
    }

    [GtkCallback]
    private void on_volume_changed_cb () {
        volume = volume_button.value;
    }

    public override bool draw (Cairo.Context cr) {
        Gtk.Allocation allocation;
        get_allocation (out allocation);

        var center_x = allocation.width / 2;
        var center_y = allocation.height / 2;
        var radius = 44;
        var line_width = 0;

        cr.save ();
        cr.move_to (center_x + radius, center_y);

        cr.set_line_width (line_width);
        cr.arc (center_x, center_y, radius - (line_width / 2), 0, 2 * Math.PI);
        cr.stroke ();

        if (state == State.PLAYING) {
            draw_progress (cr, center_x, center_y, radius);
        }

        cr.restore ();

        return base.draw (cr);
    }

    public void draw_progress (Cairo.Context cr, int center_x, int center_y, int radius) {
        var line_width = 4;
        var color = Gdk.RGBA ();
        color.parse ("#3584E4");

        cr.arc (center_x, center_y, radius - (line_width - 1) / 2, 1.5 * Math.PI, (1.5 + (progress * 2)) * Math.PI);
        Gdk.cairo_set_source_rgba (cr, color);
        cr.set_line_width (line_width);

        cr.stroke ();
    }

    [GtkCallback]
    public void start_playing () {
        state = State.PLAYING;
        beat = 0;
        timer.start_timer ();
        start_progress_timer ();
    }

    [GtkCallback]
    public void stop_playing () {
        state = State.STOPPED;
        timer.stop_timer ();
        GLib.Source.remove (timeout_id);
        progress_timer.stop ();
        progress = 0;
        queue_draw ();
    }

    [GtkCallback]
    public void on_beat_changed_cb () {
        if (beat == 0 || beat == 1) {
            progress = 0;
            progress_timer.start ();
        }
    }

    [GtkCallback]
    public void on_bpm_changed_cb () {
        if (state == State.PLAYING) {
            timer.bpm = bpm;
            stop_playing ();
            start_playing ();
        }
    }

    [GtkCallback]
    public void on_cpb_changed_cb () {
        if (state == State.PLAYING) {
            stop_playing ();
            timer.cpb = cpb;
            start_playing ();
        }
    }

    public void start_progress_timer () {
        timeout_id = GLib.Timeout.add (30, () => {
            var e = progress_timer.elapsed ();
            var span = ((60 / (bpm * 1.0))) * bpb;

            if (e >= span) {
                progress = 0;
                progress_timer.reset ();
            }
            else {
                progress = e / span;
                queue_draw ();
            }
            return GLib.Source.CONTINUE;
        });
    }
}

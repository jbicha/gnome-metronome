/* main.vala
 *
 * Copyright 2020 João Vieira <jvieira.tpt@gmail.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 */


public static int main (string[] args) {
    Gst.init (ref args);
    Hdy.init (ref args);

    typeof (Metronome.BPMControlBox).ensure ();
    typeof (Metronome.BPMScale).ensure ();
    typeof (Metronome.HeaderBar).ensure ();
    typeof (Metronome.MediaControlBox).ensure ();
    typeof (Metronome.RhythmBox).ensure ();
    typeof (Metronome.ShortcutsWindow).ensure ();
    typeof (Metronome.TapButton).ensure ();
    typeof (Metronome.TempoBox).ensure ();
    typeof (Metronome.Window).ensure ();

    var app = new Metronome.Application ();

    return app.run (args);
}

/* timer.vala
 *
 * Copyright 2020 João Vieira <jvieira.tpt@gmail.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 */


public class Metronome.Timer : GLib.Object {
    private uint timeout_id;

    public double volume { get; set; default = 1; }
    public uint beat { get; set; default = 0; }
    public uint bpm { get; set; default = 100; }
    public uint bpb { get; set; default = 4; }
    public uint cpb { get; set; default = 1; }
    private Gst.Element pipeline;

    public void start_timer () {
        beat = (beat % bpb) + 1;
        var i = 1;

        play (volume, beat == 1 && i == 1);
        timeout_id = GLib.Timeout.add (60000 / bpm / cpb, () => {
            if (i < cpb) {
                pipeline.set_state (Gst.State.NULL);
                i++;
                play (volume, beat == 1 && i == 1);
            }
            else {
                pipeline.set_state (Gst.State.NULL);
                beat = (beat % bpb) + 1;
                i = 1;
                play (volume, beat == 1 && i == 1);
            }
            return GLib.Source.CONTINUE;
        });
    }

    public void stop_timer () {
        pipeline.set_state (Gst.State.NULL);
        GLib.Source.remove (timeout_id);
    }

    public void play (double volume, bool first_beep) {
        string command;

        if (first_beep) {
            command = "playbin uri=resource:///com/jvieira/tpt/Metronome/sounds/beep_high.ogg volume=%g".printf (volume);
        }
        else {
            command = "playbin uri=resource:///com/jvieira/tpt/Metronome/sounds/beep_low.ogg volume=%g".printf (volume);
        }

        try {
            pipeline = Gst.parse_launch (command);
        }
        catch (Error e) {
            stderr.printf ("Error: %s\n", e.message);
        }

        pipeline.set_state (Gst.State.PLAYING);
    }
}

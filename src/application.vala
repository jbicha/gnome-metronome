/* application.vala
 *
 * Copyright 2020 João Vieira <jvieira.tpt@gmail.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 */


public class Metronome.Application : Gtk.Application {
    private Gtk.ApplicationWindow window;

    private const ActionEntry[] action_entries = {
        { "about", about },
    };

    public Application () {
        Object (
            application_id: "com.jvieira.tpt.Metronome",
            flags: ApplicationFlags.FLAGS_NONE
        );
    }

    construct {
        add_action_entries (action_entries, this);
    }

    protected override void activate () {
        window = new Metronome.Window (this);
        add_window (window);
    }

    protected override void startup () {
        base.startup ();
        var screen = Gdk.Screen.get_default ();
        var provider = new Gtk.CssProvider ();
        provider.load_from_resource ("/com/jvieira/tpt/Metronome/Adwaita.css");
        Gtk.StyleContext.add_provider_for_screen (screen, provider, 600);
    }

    private void about () {
        Gtk.AboutDialog about_dialog = new Gtk.AboutDialog ();
        about_dialog.destroy_with_parent = true;
        about_dialog.transient_for = window;
        about_dialog.modal = true;

        about_dialog.program_name = "Metronome";
        about_dialog.comments = "A Metronome for GNOME";
        about_dialog.version = "0.1.1";
        about_dialog.logo_icon_name = "com.jvieira.tpt.Metronome";

        about_dialog.website = "https://gitlab.gnome.org/jvieira.tpt/metronome";
        about_dialog.website_label = "Metronome";

        about_dialog.license_type = Gtk.License.GPL_3_0;

        about_dialog.authors = Credits.AUTHORS;
        about_dialog.artists = Credits.ARTISTS;

        about_dialog.present_with_time (Gtk.get_current_event_time ());
    }
}
